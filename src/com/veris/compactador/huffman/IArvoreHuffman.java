package com.veris.compactador.huffman;

public interface IArvoreHuffman {
	public String[] montar(int[] frequenciaBytes);
	
	public Node getRaiz();
}
