package com.veris.compactador.huffman;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.veris.compactador.util.BitUtil;

public class Huffman implements IHuffman {

	private String[] tabelaCoficacao;
	public static final int NUMERO_BYTES = 256;
	public static final int TAMANHO_BYTE = 7;
	public static final int CODIFICACAO_DIREITA = 1;
	public static final int CODIFICACAO_ESQUERDA = 0;
	public static final String EXTENSAO = ".huf";

	@Override
	public void codificar(InputStream arquivoOrigem, OutputStream arquivoDestino) {
		// Pega o horario que o metodo foi chamado.
		long tempoInico = System.nanoTime();

		IArvoreHuffman arvoreHuffman = new ArvoreHuffman();
		BitUtil bitUtil = new BitUtil();

		// Cria um leitor eficiente de arquivos, o qual le parte por parte ao
		// inv�z de jogar tudo na mem�ria RAM, para isso ele faz uso de um
		// buffer. tipo, se n�o usar o "bis" o algoritmo ta rodando aqui em 60s,
		// usando o bis, roda em 1,5s. Ent�o se n�o entendeu como que ele
		// funciona, entenda que com ele o seu programa vai rodar muito mais
		// rapido!!! hehe Detalhe, n�o achei nenhum lugar na internet falando a
		// respeito, tive que ca�ar na m�o.
		BufferedInputStream bis = new BufferedInputStream(arquivoOrigem);
		DataInputStream leitorArquivo = new DataInputStream(bis);

		// Cria um escritor eficiente de arquivos, o qual escreve byte a
		// byte e libera da mem�ria RAM (nos permitindo escrever arquivos
		// gigantes. Novamente ele escreve em um buffer e s� ent�o salva no
		// arquivo, sendo mais eficiente em performance. Aqui � a mesma coisa
		// que eu citei acima.
		BufferedOutputStream bos = new BufferedOutputStream(arquivoDestino);
		DataOutputStream escritorArquivo = new DataOutputStream(bos);

		// Cria um array de bytes que ir� guardar a frequencia de cada byte no
		// arquivo lido.
		int[] frequenciaBytes = new int[NUMERO_BYTES];

		// Trata as exce��es ao tentar ler e/ou escrever em um arquivo.
		try {
			// Salva o tamanho original do arquivo em um inteiro.
			int tamanhoArquivoOriginal = leitorArquivo.available();

			// Salva o inicio do arquivo para que possamos ler novamente o
			// arquivo.
			leitorArquivo.mark(tamanhoArquivoOriginal + 1);

			/*
			 * Le o arquivo atrav�s de um for. Note que na internet, voc� ir�
			 * encontra algo como while (leitorArquivo.avaliable() != 0); Se
			 * usassemos desta forma, o algoritmo iria demorar 30 vezes mais
			 * para rodar. Usando um for e salvando o tamanho do arquivo em uma
			 * variavel, o algoritmo � 30 vezes mais eficaz!!! ;)
			 */
			for (int i = 0; i < tamanhoArquivoOriginal; i++) {
				// Adiciona o byte atual no nosso array de frequencia.
				frequenciaBytes[leitorArquivo.read()]++;
			}

			// Volta o ponteiro interno do leitorArquivo para o primeiro byte.
			leitorArquivo.reset();

			// Monta a nossa arvore de huffman e retorna um array que ir� nos
			// auxiliar na busca de codifica��o de cada byte. Qualquer coisa,
			// leia a descri��o no arquivo "ArvoreHuffman" no m�todo "montar"
			tabelaCoficacao = arvoreHuffman.montar(frequenciaBytes);

			// J� escreve no cabe�alho do arquivo o array de frequencia de cada
			// byte (para quando formos descompactar conseguirmos montar a mesma
			// arvore que montamos agora.
			for (int i = 0; i < frequenciaBytes.length; i++) {
				escritorArquivo.writeInt(frequenciaBytes[i]);
			}

			// ser� o nosso ponteiro que ir� apontar em qual posi��o do byte
			// estamos escrevendo (a nivel de bit).
			int pos = TAMANHO_BYTE;

			// cria um novo byte zerado (0000.0000) para escrevermos a
			// codifica��o atual.
			byte atual = 0;

			// novamente le o arquivo do zero.
			for (int i = 0; i < tamanhoArquivoOriginal; i++) {
				// recebe um array de boolean que indica qual � a codifica��o do
				// caracter passado. Se a posicao "j" do codificacaoByteAtual
				// for TRUE, ent�o o bit deve ser "1", se for FALSE o bit deve
				// ser "0".
				boolean[] codificaoByteAtual = codificarByte(leitorArquivo
						.read());

				// para cada bit de codifica��o vindo da tabela de codifica��o,
				// escreve o bit no nosso byte atual.
				for (int j = 0; j < codificaoByteAtual.length; j++) {
					// Se a codifica��o na posi��o "j" do array for TRUE
					if (codificaoByteAtual[j]) {
						// escreve um bit na posi��o que o nosso ponteiro est�
						// apontando no array. Por exemplo, digamos que o
						// primeiro caracter possua a codificacao "011" e j�
						// gravamos no nosso byte os dois primeiros bits. Neste
						// caso teremos o byte atual como "0 1 0* 0 0 0 0 0",
						// sendo o "*" o nosso ponteiro "pos". Como ainda temos
						// que escrever o bit "1", vamos escreve-lo na posi��o
						// do nosso ponteiro: "0 1 1* 0 0 0 0 0".
						atual = bitUtil.escrever(atual, pos);
					}

					// Se n�o tivermos mais nenhum byte para ler no nosso leitor
					// de arquivos E j� tivermos gravado todos os bits deste
					// ultimo byte, ent�o devemos gravar o byte da forma que
					// est� (pois mais abaixo voc� ver� que ele s� grava o byte
					// quando o byte est� cheio). Tipo, o ultimo byte da
					// codifica��o vai existir um monte de "bits" vazios, pois
					// nem sempre da para ter a codifica��o sendo um numero
					// divisivel por 8. Bom, se n�o entendeu, s� � importante
					// entender que o ultimo byte pode ter um monte de bits
					// vazios, ent�o � preciso salv�-lo mesmo sem ele estar
					// completo e salvarmos esta posi��o no nosso cabe�alho.
					// Bom, na descompacta��o voc�s v�o entender melhor porque.
					if (i == tamanhoArquivoOriginal - 1
							&& j == codificaoByteAtual.length - 1) {
						// escreve o byte atual no arquivo.
						escritorArquivo.write(atual);

						// sai do loop
						break;
					} else if (pos == 0) {
						// Se o nosso ponteiro da posi��o atual de bits em um
						// byte chegou a zero, ent�o devemos resetar ele.
						pos = TAMANHO_BYTE;

						// E como o nosso byte ficou completamente populado,
						// vamos salv�-lo no arquivo.
						escritorArquivo.write(atual);

						// Reseta o byte atual como um byte novo zerado
						// (0000.0000).
						atual = 0;
					} else {
						// Caso contr�rio, altera o valor do nosso ponteiro para
						// apontar para o pr�ximo bit. No nosso exemplo
						// anterior, tinhamos isto: "0 1 1* 0 0 0 0 0", ent�o
						// agora o ponteiro iria apontar para o pr�ximo bit
						// disponivel, ou seja: "0 1 1 0* 0 0 0 0".
						pos--;
					}
				}
			}

			// Pronto, j� lemos o arquivo inteiro, compactamos ele e salvamos
			// ele em um arquivo novo. Agora s� falta fechar os arquivos. Mas
			// antes de fechar, devemos salvar qual foi o ultimo bit valido no
			// nosso arquivo compactado.
			escritorArquivo.write(pos);

			// fecha os arquivos para evitar deix�-lo travado (manja quando voc�
			// tenta deletar um arquivo e diz que ele est� em uso? Se voc� n�o
			// usar este .close, � o que ir� acontecer com o teu arquivo...)
			leitorArquivo.close();

			// No caso do escritor, antes de fechar o comando .close() salva o
			// arquivo, para s� ent�o fech�-lo.
			escritorArquivo.flush();
			escritorArquivo.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Pega o horario em que o programa acabou de compactar.
		long tempoFinal = System.nanoTime();

		// Calcula o tempo de execu��o em segundos.
		Double tempoExec = (tempoFinal - tempoInico) * Math.pow(10, -9);
		System.out.println("Tempo de execu��o: " + tempoExec);
	}

	@Override
	public void decodificar(InputStream arquivoOrigem,
			OutputStream arquivoDestino) {
		// Pega o horario que o metodo foi chamado.
		long tempoInico = System.nanoTime();

		IArvoreHuffman arvoreHuffman = new ArvoreHuffman();
		BitUtil bitUtil = new BitUtil();

		// Cria um leitor eficiente de arquivos, o qual le parte por parte ao
		// inv�z de jogar tudo na mem�ria RAM.
		BufferedInputStream bis = new BufferedInputStream(arquivoOrigem);
		DataInputStream leitorArquivo = new DataInputStream(bis);

		// Cria um escritor eficiente de arquivos, o qual escreve byte a
		// byte e libera da mem�ria RAM (nos permitindo escrever arquivos
		// gigantes.
		BufferedOutputStream bos = new BufferedOutputStream(arquivoDestino);
		DataOutputStream escritorArquivo = new DataOutputStream(bos);

		// Cria um array vazio o qual conter� a frequencia de bytes salva no
		// cabe�alho no nosso arquivo.
		int[] frequenciaByte = new int[NUMERO_BYTES];

		try {
			// Le os 256 primeiros ints e os salvam no nosso array de
			// frequencia.
			for (int i = 0; i < NUMERO_BYTES; i++) {
				frequenciaByte[i] = leitorArquivo.readInt();
			}

			// monta a arvore de huffman de acordo com o nosso array de
			// frequencia
			arvoreHuffman.montar(frequenciaByte);

			// Cria um ponteiro que ir� nos auxiliar na codifica��o, apontando
			// para os n�s at� chegar em um n� folha
			Node ponteiro = arvoreHuffman.getRaiz();

			// inicializa a valiavel ultimoBitValido como -1.
			int ultimoBitValido = -1;

			int tamanhoArquivoCodificado = leitorArquivo.available();
			// le o arquivo compactado
			for (int i = 0; i < tamanhoArquivoCodificado - 1; i++) {
				// le o byte atual
				int atual = leitorArquivo.read();

				// Se s� falta 1 byte a ser lido, ent�o s� falta o nosso
				// indicador de qual � o ultimo bit v�lido. Lembre-se que um
				// vetor de 3 posi��es come�a no 0 e termina no 2, ent�o aqui �
				// a mesma regra. tanto � que o nosso i come�a em 0 e termina em
				// tamanho -1... ent�o se o ultimo � tamanho-1, o penultimo �
				// tamanho-2 ;)
				if (i == tamanhoArquivoCodificado - 2) {
					ultimoBitValido = leitorArquivo.read();
				}

				// le bit a bit para ir decodificando.
				for (int j = TAMANHO_BYTE; j >= 0; j--) {
					// verifica se o bit lido � "1" (direita).
					boolean isDireita = bitUtil.ler(atual, j) == CODIFICACAO_DIREITA;

					// Se for a codifica��o da direita, ele le o filho direito
					// da arvore.
					if (isDireita) {
						ponteiro = ponteiro.getFilhoDireito();
					} else {
						// Caso contrario, le o filho esquerdo da arvore.
						ponteiro = ponteiro.getFilhoEsquerdo();
					}

					// Se o n� para o qual o ponteiro est� apontando for um n�
					// folha, escreve o byte equivalente do caracter original no
					// arquivo novo.
					if (ponteiro.isFolha()) {
						// Escreve o byte decodificado no arquivo novo
						escritorArquivo.write(ponteiro.getChave());

						// Volta o ponteiro para a raiz da arvore de huffman.
						ponteiro = arvoreHuffman.getRaiz();
					}

					// Verifica que estamos no ultimo byte a ser lido e que o
					// ultimo bit valido tb j� foi lido. Como quando faltavam 2
					// bytes a serem lidos, decidimos ler na mesma execu��o,
					// temos que compara com "-2" ao tamanho do arquivo.
					if (i == tamanhoArquivoCodificado - 2
							&& j == ultimoBitValido) {
						// neste caso, para o loop, pois os proximos bits s�o
						// nulos.
						break;
					}
				}
			}

			// fecha os arquivos para evitar deix�-lo travado (manja quando voc�
			// tenta deletar um arquivo e diz que ele est� em uso? Se voc� n�o
			// usar este .close, � o que ir� acontecer com o teu arquivo...)
			leitorArquivo.close();

			// No caso do escritor, antes de fechar o comando .close() salva o
			// arquivo, para s� ent�o fech�-lo.
			escritorArquivo.flush();
			escritorArquivo.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Pega o horario em que o programa acabou de compactar.
		long tempoFinal = System.nanoTime();

		// Calcula o tempo de execu��o em segundos.
		Double tempoExec = (tempoFinal - tempoInico) * Math.pow(10, -9);
		System.out.println("Tempo de execu��o: " + tempoExec);
	}

	/**
	 * Codifica um caractere em uma sequencia booleana representando a nossa
	 * codifica��o "01010" ;)
	 */
	private boolean[] codificarByte(int caractere) {
		// Busca a codifica��o em String que salvamos na tabela de codifica��o.
		String aux = tabelaCoficacao[caractere];

		// Se a String retornada estiver vazia, n�o retorna nada.
		if (aux.length() <= 0) {
			return null;
		}

		// Se achou uma codifica��o, ent�o cria um novo array com o tamanho de
		// caracteres que existem na string.
		boolean[] codificacao = new boolean[aux.length()];

		// Quebra cada peda�o do caracter e joga no array TRUE se for 1, ou
		// FALSE se for 0. Exempplo, digamos que a codifica��o de "A" seja
		// "011", ele vai quebrar a string em 3 peda�os: "0", "1" e "1", e ent�o
		// vai colocar os valores no array da seguinte forma: 
		// codificacao[0] = false (pois o primeiro � "0")
		// codificacao[1] = true (pois o segundo � "1")
		// codificacao[2] = true (pois o terceiro tb � "1")
		for (int i = 0; i < aux.length(); i++) {
			codificacao[i] = "1".equals(aux.substring(i, i + 1));
		}

		// retorna este array de codifica��o
		return codificacao;
	}

}