package com.veris.compactador.huffman;

import java.io.InputStream;
import java.io.OutputStream;

public interface IHuffman {

	public void codificar(InputStream arquivoOrigem, OutputStream arquivoDestino);

	public void decodificar(InputStream arquivoOrigem,
			OutputStream arquivoDestino);
}
