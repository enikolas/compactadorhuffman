/**
 * 
 */
package com.veris.compactador.huffman;

/**
 * @author enikolas
 * 
 */
public class Node {
	// Guarda o byte de um caracter do arquivo
	private int chave;
	// Guarda a frequencia deste caracter no arquivo.
	private int frequencia;
	
	// Ponteiro para o pr�ximo da lista ligada.
	private Node proximo;
	
	// Ponteiro para o filho esquerdo da arvore de huffman.
	private Node filhoEsquerdo;
	
	// Ponteiro para o filho direito da arvore de huffman.
	private Node filhoDireito;

	/**
	 * @return the chave
	 */
	public int getChave() {
		return chave;
	}

	/**
	 * @param chave
	 *            the chave to set
	 */
	public void setChave(int chave) {
		this.chave = chave;
	}

	/**
	 * @return the frequencia
	 */
	public int getFrequencia() {
		return frequencia;
	}

	/**
	 * @param frequencia
	 *            the frequencia to set
	 */
	public void setFrequencia(int frequencia) {
		this.frequencia = frequencia;
	}

	/**
	 * @return the proximo
	 */
	public Node getProximo() {
		return proximo;
	}

	/**
	 * @param proximo
	 *            the proximo to set
	 */
	public void setProximo(Node proximo) {
		this.proximo = proximo;
	}

	/**
	 * @return the filhoEsquerdo
	 */
	public Node getFilhoEsquerdo() {
		return filhoEsquerdo;
	}

	/**
	 * @param filhoEsquerdo
	 *            the filhoEsquerdo to set
	 */
	public void setFilhoEsquerdo(Node filhoEsquerdo) {
		this.filhoEsquerdo = filhoEsquerdo;
	}

	/**
	 * @return the filhoDireito
	 */
	public Node getFilhoDireito() {
		return filhoDireito;
	}

	/**
	 * @param filhoDireito
	 *            the filhoDireito to set
	 */
	public void setFilhoDireito(Node filhoDireito) {
		this.filhoDireito = filhoDireito;
	}

	/**
	 * Valida se o n� passado � folha.
	 */
	public boolean isFolha() {
		// Ao invez de eu salvar o retorno em uma variavel para s� ent�o
		// retorn�-la, eu ja fiz a verifica��o na msm linha do retorno poupando
		// variavel e processamento a toa. Aqui ele retorna TRUE quando os dois
		// filhos forem NULL e FALSE quando N�O forem. Ou seja, a defini��o de
		// n� folha!!! (Um n� s� � folha quando ele n�o possui filhos).
		return filhoDireito == null && filhoEsquerdo == null;
	}
}
