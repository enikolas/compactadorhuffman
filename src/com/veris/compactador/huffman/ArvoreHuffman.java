package com.veris.compactador.huffman;


/**
 * Arvore de Huffman.
 */
public class ArvoreHuffman implements IArvoreHuffman {
	/**
	 * Lista ligada para auxiliar a montagem da arvore.
	 */
	private IListaLigada listaLigada = new ListaLigada();

	/**
	 * N� raiz da arvore.
	 */
	private Node raiz;

	/**
	 * Array contendo a codifica��o de cada byte.
	 */
	private String[] tabelaCodificacao;

	/**
	 * Monta a arvore de huffman recebendo um array que contem a frequencia de
	 * cada byte no arquivo.
	 * 
	 * @param requenciaBytes
	 *            - um array com a frequencia que cada byte aparece no arquivo,
	 *            exemplo: o byte equivalente a letra "A" aparece 700 vezes no
	 *            arquivo, isso seria
	 */
	@Override
	public String[] montar(int[] frequenciaBytes) {
		// Le um array que cont�m a frequencia de cada byte no arquivo passado
		for (int i = 0; i < frequenciaBytes.length; i++) {
			// Verifica se o contador da frequencia � maior que zero
			if (frequenciaBytes[i] > 0) {
				// Se for maior que zero, cria um novo n�
				Node node = new Node();
				// Coloca na "chave" do n� o byte que representa uma letra no
				// arquivo.
				// (procure no google a respeito da tabela ascii para entender
				// melhor a respeito).
				node.setChave(i);
				// Coloca a "frequ�ncia" do byte no n�.
				node.setFrequencia(frequenciaBytes[i]);
				// Insere o n� na lista ligada.
				listaLigada.inserirOrdenado(node);
			}
		}

		// Obt�m o primeiro n� da nossa lista ligada e o coloca na raiz da
		// �rvore.
		raiz = listaLigada.getInicio();

		// Enquanto a raiz n�o for nula e a raiz estiver apontando para um
		// pr�ximo elemento
		while (raiz != null && raiz.getProximo() != null) {
			// Remove o �ltimo elemento da lista ligada e o coloca na vari�vel
			// "ultimo"
			Node ultimo = listaLigada.removerUltimo();
			// Remove agora o �ltimo elemento da lista ligada e o coloca na
			// vari�vel "penultimo".
			// Pensa assim, em uma lista listaga vc tem algo como: "inicio" ->
			// "penultimo" -> "ultimo"
			// Quando voc� remove o "ultimo", quem passa a ser o ultimo � o
			// "penultimo" ;)
			Node penultimo = listaLigada.removerUltimo();

			// Cria o novo n�
			Node node = new Node();
			// Coloca na frequencia de bytes do novo n� a frequencia somada dos
			// dois ultimos:
			// ultimo.frequencia + penultimo.frequencia
			node.setFrequencia(ultimo.getFrequencia()
					+ penultimo.getFrequencia());
			// Coloca o n� "ultimo" como filhoDireito do novo n� da arovre
			// criado
			node.setFilhoDireito(ultimo);
			// Coloca o n� "penultimo" como filhoEsquerdo do novo n� da arvore
			// criado
			node.setFilhoEsquerdo(penultimo);

			// Insere o novo n� criado de forma ordenada na lista ligada.
			listaLigada.inserirOrdenado(node);

			// Obtem o primeiro n� da lista ligada.
			raiz = listaLigada.getInicio();
		}

		// Cria um array de Strings que ir� conter todas as codifica��es para
		// cada byte.
		// Exemplo: tabelaCodificacao["A"] = "0"; tabelaCodificacao["B"] = "10";
		// tabelaCodificacao["C"] = "11";
		// Mas l�gico, a codifica��o vai depender de como que a arvore foi
		// montada. Para intender melhor, veja as figuras que eu mandei no email
		// explicando arvore de huffman.
		tabelaCodificacao = new String[Huffman.NUMERO_BYTES];

		// Agora que j� foi criado um array em branco com a quantidade m�xima de
		// caracteres possiveis que nosso arquivo pode ter, ele preenche a
		// tabela de codifica��o da forma como foi explicada acima. Para
		// intender melhor como que ele preenche esta tabela, v� at� a
		// implementa��o deste m�todo (clice em cima do nome do metodo e depois
		// pressione a tecla F3).
		preencheTabelaCodificacao(raiz, "");

		// Agora que a tabela foi completamente populada, retorna esta tabela
		// para facilitar a vida de quem for compactar um arquivo txt.
		return tabelaCodificacao;
	}

	/**
	 * Este m�todo � quem � respons�vel por preencher o nosso array que facilita
	 * a busca da codifica��o de cada caracter em bits. Sem esse array, seria
	 * necess�rio percorrer a arvore inteira para descobrir a codifica��o de
	 * cada caractere, o que demandaria um tempo absurdo. Ent�o aqui est� algo
	 * que o professor pode perguntar:
	 * "porque voc�s usaram um array com a codifica��o?" Resposta:
	 * "por quest�es de performance. desta forma o algoritmo � mais eficiente e r�pido."
	 * Segunda pergunta: "Que outra op��o voc� teria para fazer isso?" Segunda
	 * resposta:
	 * "fazer uma busca na arvore inteira cada vez que eu quisesse descobri a codifica��o de uma letra no arquivo."
	 * 
	 * @param node
	 * @param codificacao
	 */
	private void preencheTabelaCodificacao(Node node, String codificacao) {
		// Verifica��o b�sica se o n� passado no m�todo recursivo n�o est� nulo.
		// Isto � para evitar erros do java, pois se vc tem uma variavel que �
		// nula e tenta usar qualquer atributo desta variavel, vai dar um erro
		// no java. Na pr�tica, pense nesta linha como uma condi��o de parada no
		// nosso m�todo recursivo caso tenha acontecido um erro no algoritmo.
		if (node == null) {
			// Se o n� � nulo, retorna a execu��o a quem chamou o m�todo
			// recursivo.
			// Note que este m�todo possui o tipo do retorno como "void", o que
			// siginifica que o m�todo n�o deve retornar NADA. E � exatamente
			// isto o que ele esta retornando... hehe "return ;" ;)
			return;
		}

		// Verifica se o n� passado � um n� folha (n� folha � um n� que n�o
		// possui nenhum filho).
		// Essa � a nossa condi��o de parada principal deste m�todo recursivo,
		// sem essa verifica��o, aconteceria um loop infinito. por quest�es de
		// boas pr�ticas, eu coloquei a condi��o de parada logo no inicio do
		// m�todo recursivo, assim fica mais facil de entender o c�digo.
		if (node.isFolha()) {
			// Se o n� � folha, significa que ele � uma letra do nosso arquivo,
			// ent�o vamos colocar essa letra no nosso array de codifica��es:
			// byte "a" = 97, ent�o, tabelaCodificacao[97] = codificacao (a
			// string
			// codificacada que foi passada no nosso metodo recursivo).
			// O que cont�m nesta string "codificacao" voc�s ir�o intender mais
			// abaixo. Como eu falei, � uma boa pr�tica colocar a condi��o de
			// parada de um m�todo recursivo logo no inicio.
			tabelaCodificacao[node.getChave()] = codificacao;

			// Novamente estou usando o return vazio, que � para RETORNAR a
			// EXECU��O para QUEM CHAMOU este m�todo. (estou tentando usar as
			// palavras do professor de estrutura de dados... hehe)
			return;
		}

		// Verifica se o n� atual possui algum filho na direita.
		if (node.getFilhoDireito() != null) {
			// Agora � que a m�gica come�a!!! Se o n� possui um filho a direita,
			// ent�o a codifica��o desse n� a direita ser� a codifica��o do n�
			// atual + "1" (que � o valor definido na constante
			// CODIFICACAO_DIREITA). Vamos usar um exemplo ent�o... Usem a
			// imagem quem eu mandei no email em que eu explico arvore de
			// huffman. Digamos que o n� atual seja o "* -> 2", logo a
			// codifica��o dele � 0->1 (pois tivemos que ir um n� a esquerda na
			// raiz, e depois um n� a direita para s� ent�o chegarmos nele).
			// Ent�o supondo que nesta execu��o, o Node node passado seja esse
			// que eu mencionei. a string codificao passada no m�todo ser� "01"
			// e a codifica��o do filho direito deste n�, no caso, a codifica��o
			// de "E" ser� a codificao atual + "1", ou seja: "01"+"1" = "011". �
			// exatamente isto que esta linha esta fazendo.
			String codificacaoDireita = codificacao
					+ Huffman.CODIFICACAO_DIREITA;

			// Agora que eu sei a codifica��o do meu filho direito, vou chamar
			// recursivamente este m�todo at� que o n� atual seja folha (a
			// condi��o de parada que eu havia explicado logo acima).
			preencheTabelaCodificacao(node.getFilhoDireito(),
					codificacaoDireita);
		}

		// O c�digo s� ir� chegar a executar est� linha, quando todas as
		// chamadas recursivas anteriores tiverem sido resolvidas. Mas o que
		// isso significa? Bom, isso significa que na primeira execu��o deste
		// m�todo, esta linha abaixo s� ser� executada quando todos os n�s a
		// direita j� tiverem sido lidos. bom, acho que estou complicando
		// demais, ent�o vamos descomplicar usando a imagem do email l�. Esta
		// primeira linha s� ser� executada, quando o c�digo acima tiver lido
		// todos os n�s a direita, que no caso, � apenas o n� "O -> 4" e tiver
		// voltado para quem chamou, que foi a execu��o que possuia o node
		// apontado para a raiz (na primeira execu��o do m�todo agente passa a
		// raiz da arvore). Ent�o nesta linha temos o node = "*	-> 9" e a
		// codifica��o dele � em branco ou "" (tanto faz... hehe).
		if (node.getFilhoEsquerdo() != null) {
			// Aqui � o mesmo caso que a codificacao da direita, por�m neste
			// caso � a codifica��o para a esquerda e essa codifica��o foi
			// definida como CODIFICACAO_ESQUERDA = 0; logo, no exemplo
			// anterior, se tivessemos uma codifica��o de "01", a
			// codificaoEsquerda viraria: "01"+"0" = "010".
			String codificacaoEsquerda = codificacao
					+ Huffman.CODIFICACAO_ESQUERDA;
			// Chamo de formar recursiva este m�todo s� que passando o filho
			// esquerdo mais a sua codifica��o. Mesmo caso do de cima, por�m
			// aqui � o filho esquerdo.
			preencheTabelaCodificacao(node.getFilhoEsquerdo(),
					codificacaoEsquerda);
		}
	}

	/**
	 * Retorna a raiz da arvore de huffman. O professor mencionou que n�o
	 * precisa disto, por�m tenho que pensar o que posso fazer para melhorar. Se
	 * voc�s acharem melhor, eu n�o modifico e fica assim.
	 */
	@Override
	public Node getRaiz() {
		return raiz;
	}
}
