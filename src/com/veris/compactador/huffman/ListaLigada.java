package com.veris.compactador.huffman;

/**
 * Implementa��o de Lista Ligada, por�m focado em codifica��o de huffman *
 */
public class ListaLigada implements IListaLigada {
	/**
	 * N� que aponta para o primeiro n� da lista.
	 */
	private Node inicio;

	/**
	 * M�todo que insere um n� de forma ordenada na lista ligada.
	 */
	@Override
	public void inserirOrdenado(Node node) {
		// Se a lista ligada n�o possuir nenhum n� ainda, ent�o insere o n� no
		// in�cio.
		if (inicio == null) {
			inicio = node;
		} else {
			// Caso contr�rio, teremos que percorrer a lista para descobrir qual
			// � a posi��o em que devemos inserir o n� passado. Para isto eu
			// criei a vari�vel "atual" que � um ponteiro para o n� "atual" em
			// que estamos olhando.
			Node atual = inicio;

			// Ent�o enquanto o atual possuir um proximo (atual.proximo != null)
			// e a frequencia do pr�ximo do atual for maior que a do n� passado,
			// ele passa o "ponteiro", que no caso � a nossa variavel "atual"
			// para o pr�ximo. Lembrando que a nossa lista ligada �
			// "inversamente" ordenada. ou seja, o primeiro elemento � o item
			// que possui maior frequencia no nosso arquivo, enquanto o ultimo
			// elemento � o menos frequente.
			while (atual.getProximo() != null
					&& atual.getProximo().getFrequencia() > node
							.getFrequencia()) {
				atual = atual.getProximo();
			}

			// Pronto, agora que a execu��o saiu do loop, s� pode significar uma
			// de 2 op�oes.
			// Op��o A: percorremos a lista inteira, por�m todos os elementos
			// possuem uma frequencia maior do que a frequencia do n� que
			// queremos inserir. Em c�digo, isso significa que o nosso ponteiro
			// "atual" est� no ultimo elemento da lista, ou seja, n�o possue
			// nenhum atual.proximo (atual.proximo == null). Neste caso, como o
			// n� que queremos inserir � o menor, ele ser� inserido no final da
			// lista ligada.
			if (atual.getProximo() == null) {
				// Insere o n� no final da lista ligada.
				atual.setProximo(node);
			} else {
				// Op��o B: percorremos a lista e achamos a posi��o que queremos
				// inserir o nosso elemento. Mas qual posi��o � esta? Para o
				// nosso n� ser inserido de forma ordenada, o nosso ponteiro
				// "atual" precisa ter uma frequencia maior que a do n� que
				// queremos inserir, por�m o pr�ximo n� do "atual"
				// (atual.proximo) tem que ser menor ou igual a frequencia do n�
				// que queremos inserir. Se a execu��o veio parar nesse ELSE, �
				// porque caimos nesta situa��o B.
				// Sendo assim, precisamos inserir ele na lista, por�m sem
				// perder os dados, para isso, primeiro temos que apontar o
				// proximo do novo n� ao proximo do n� atual.
				node.setProximo(atual.getProximo());

				// E por fim, apontar o pr�ximo do atual, para o novo n�
				// inserido. "graficamente", ficou assim:
				// "atual -> node -> proximoAtual"
				atual.setProximo(node);
			}
		}
	}

	/**
	 * Remove o �ltimo elemento da lista ligada.
	 */
	@Override
	public Node removerUltimo() {
		// Se a nossa lista n�o possui nenhum elemento, ent�o n�o tem como
		// remover o ultimo, sendo assim, retorna nada para quem chamou largar
		// m�o de ser besta!!! hehe
		if (inicio == null) {
			return null;
		}

		// Agora sim o algoritmo come�a. Novamente teremos um ponteiro que ira
		// apontar para o n� atual. Vamos tentar deixar isso de forma "grafica":
		// *5 -> 4 -> 3
		// O "*" em indica para qual n�mero estamos apontando, no caso, o 5
		Node atual = inicio;

		// Se a nossa lista s� possuir 1 elemento, n�o precisamos perder tempo
		// "percorrendo" ela, � apenas remover o �nico elemento da lista.
		if (atual.getProximo() == null) {
			// Para isso, o inicio precisa apontar para o NADA.
			inicio = null;

			// E ent�o retornamos o n� que estava salvo na variavel "atual" (se
			// o nosso exemplo "grafico" s� possuisse um elemento, retornaria o
			// 5).
			return atual;
		}

		// Droga, tem mais de um elemento na lista. Ent�o � preciso usar um
		// segundo ponteiro para nos auxiliar (pq sen�o vc chegaria no ultimo
		// elemento, e teria que percorrer a lista tudo de novo para s� ent�o
		// remov�-lo). Por hora nosso ponteiro auxiliar n�o est� apontando para
		// nada, por�m ele ser� usado logo abaixo.
		Node aux = null;

		// Enquanto o n� atual possuir um pr�ximo elemento. Lembra do nosso
		// exemplo? o atual seria o numero 5, ent�o depois do 5 vem o 4...
		while (atual.getProximo() != null) {
			// Nesse caso, ent�o primeiro iremos apontar o ponteiro auxiliar
			// para o pr�ximo do auxiliar. Ent�o graficamente ficaria assim:
			// *5 -> 4. -> 3
			// O "." indica para qual n�mero estamos apontando com o aux, que no
			// caso � o numero 4.
			aux = atual.getProximo();

			// O n� auxiliar � o �ltimo elemento da lista? ou seja, para ele ser
			// o ultimo ele n�o pode apontar para ninguem.
			if (aux.getProximo() == null) {
				// Se ele � o �ltimo n�, ent�o paramos o loop.
				// O comando "break" para exatamente nesta linha o loop e vai
				// pular para linha "atual = atual.setProximo(null);"
				break;
			}

			// Se a execu��o est� aqui, � porque o nosso ponteiro auxiliar n�o
			// era o ultimo elemento da lista, neste casso, passamos o ponteiro
			// atual para o mesmo do aux. Graficamente:
			// 5 -> *4+ -> 3
			// Sim, nesta linha o ponteiro aux e atual est�o apontando para o
			// mesmo n�.
			atual = atual.getProximo();
		}

		// Se saimos do loop, � porque o nosso ponteiro atual est� apontando
		// para o penultimo elemento da lista. Enquanto o ponteiro auxiliar est�
		// apontando para o ultimo elemento da lista. Graficamente:
		// 5 -> *4 -> 3.
		// Sendo assim, faz o penultimo apontar para o nada.
		atual.setProximo(null);

		// E retorna o ultimo elemento.
		return aux;
	}

	/**
	 * Obtem o n� inicial da lista ligada. O professor comentou que era
	 * desnecess�rio este m�todo, por�m eu usei para facilitar.
	 */
	@Override
	public Node getInicio() {
		return inicio;
	}
}
