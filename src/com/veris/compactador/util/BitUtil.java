/**
 * 
 */
package com.veris.compactador.util;

/**
 * Este � um utilit�rio que ir� nos auxiliar a manipular BITs em java.
 */
public class BitUtil {
	/**
	 * Leitura de bits em um byte..
	 * 
	 * @param info
	 *            - O byte do qual se quer ler o bit.
	 * @param pos
	 *            - A posi��o em que se deseja ler, sendo 0 - LSB e 7 - MSB. LSB
	 *            = o mais a direita >>> e MSB = o mais a esquerda <<<.
	 *            "Graficamente" seria isso: 7 6 5 4 3 2 1 0
	 * @return O bit da posi��o desejada. Ou seja, retorna 1 se o bit na posicao
	 *         desejada for 1, ou 0 se for 0.
	 */
	public int ler(int info, int pos) {
		// Cria um byte base: 0000.0001
		byte base = 1;
		// Agora o bixo pega... hehe, vamos tentar descomplicar. Em nivel de
		// byte, o java possui o operador "<<" que � o que desloca bits em N
		// posi��es. Um exemplo do uso deste operador � "base << 1" =
		// "0000.0010". Um segundo exemplo para garantir que voc�s entenderam �:
		// "base << 5" = "0010.0000". Simples? Complicado? Bom, isso � o come�o
		// da hist�ria. ent�o "base << pos" vai deslocar o "1", "pos" vezes.
		// Certo, se ainda tiverem duvida nesta parte, podem me mandar um email
		// que eu tendo explicar melhor. O outro operador que nos auxilia a
		// trabalhar com BIT's � o operador "&" (E). Note que o "E" de true e
		// false � "&&" enquanto o "E" bin�rio � "&" (um unico sinal de E
		// comercial). Ok, mas o que esse operador faz? Ele faz compara��es "E"
		// para cada bit. Vamos para um exemplo para voc�s conseguirem
		// vizualizar isto. Ex1.: "1111.1111 & 0010.0000" = "0010.0000". (ou
		// seja, ele s� retorna "1" quando existe "1" em ambos os lados). Vamos
		// ao pr�ximo exemplo para enfatizar isso. Ex2: "1100.0011 & 0011.1100"
		// = "0000.0000". Como voc�s podem notar, como n�o existem nenhum "1" na
		// mesma posi��o em ambas as posi��es, � retornado tudo zero. Bom,
		// assumindo que voc�s entenderam isto, vamos a explica��o de como eu
		// leio um 1 bit em uma determinada posi��o. Bom, eu fa�o uso desse
		// operador "&", mas antes eu desloco o 1 na posi��o que eu quero:
		// "base << pos", e ent�o comparo o retorno disto com o byte que me foi
		// passado: "(base << pos) & info". Ent�o como pode ser visto no exemplo
		// 2, se o bit na posi��o que eu quero n�o estiver "ligado" no byte
		// passado, a opera��o "&" ir� retornar "0000.0000", convertendo isso em
		// inteiro, retornar� "0". Ent�o eu comparo se o valor retornado � zero.
		// se for zero retorna zero, se for maior que zero, retorna 1.
		return ((base << pos) & info) > 0 ? 1 : 0;
	}

	/**
	 * Escrita de bits em um byte.
	 * 
	 * @param info
	 *            - O byte do qual se quer escrever um bit.
	 * @param pos
	 *            - A posi��o em que se deseja escrever, sendo 0 - LSB e 7 - MSB
	 * @return O byte em que a informa��o foi escrita.
	 */
	public byte escrever(byte info, int pos) {
		// Mesma coisa do que o m�todo acima. cria um byte com apenas 1 bit
		// ligado: "0000.0001".
		byte base = 1;
		// Antes de explicar a linha inteira, vou explicar o terceiro operador
		// binario do java. o "|" ("ou" binario). Ele funciona quase igual ao
		// "&", tirando o fato que agora ele faz opera��es de "ou". ;) Ex.:
		// "1100.0011 | 0001.1000" = "1101.1011". Logo, ele retorna todas as
		// posi��es que est�o ligadas, independente de qual o lado. Certo, uma
		// vez entendido isso, fica f�cil entender como que se escreve um BIT em
		// uma determinada posi��o!!! Por exemplo, se eu quiser escrever "1" na
		// posi��o 5 do byte (lembrando as posi��es s�o: 76543210).
		return (byte) ((base << pos) | info);
	}
}
