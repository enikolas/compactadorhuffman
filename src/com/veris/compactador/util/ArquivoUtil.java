package com.veris.compactador.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Classe que � respons�vel por manipular arquivos.
 */
public class ArquivoUtil {
	/**
	 * Le um arquivo.
	 * 
	 * @param caminhoArquivo
	 *            - o caminho em que se encontra o arquivo. Ex.:
	 *            "C:\arquivo.txt"
	 * @return
	 * @throws IOException
	 */
	public InputStream ler(String caminhoArquivo) throws IOException {
		// Isso aqui � decoreba mesmo. Aqui � criado um referencia para o
		// arquivo. O File em si n�o consegue ler ou escrever em um arquivo, ele
		// apenas tem a estrutura do mesmo, por�m � pr�-requisito para o cara
		// que sabe como se le um arquivo.
		File file = new File(caminhoArquivo);

		// Este � o cara que sabe ler os bytes de um arquivo. Novamente, �
		// decoreba, j� que estamos usando as facilidades da linguagem java.
		InputStream is = new FileInputStream(file);

		// Para o algoritmo n�o precisar ler o arquivo inteiro na mem�ria RAM,
		// estamos passando o cara que sabe ler o arquivo em partes pequenas.
		return is;
	}

	/**
	 * Escreve um array de bytes em um arquivo.
	 * 
	 * @param caminhoArquivo
	 *            - o caminho do arquivo em que queremos salvar. Ex.:
	 *            "C:\teste.txt.huf"
	 * @param arquivoByte
	 *            - o array de bytes que queremos salvar.
	 * @throws IOException
	 */
	public OutputStream escrever(String caminhoArquivo, byte[] arquivoByte)
			throws IOException {
		// Igual ao de cima, cria uma referencia em java para o arquivo passado.
		File file = new File(caminhoArquivo);

		// Este � o cara que sabe como que se escreve bytes em um arquivo.
		OutputStream os = new FileOutputStream(file);

		// Para poupar mem�ria RAM, passa o cara que sabe escrever em um arquivo
		// para que possamos salvar byte a byte no arquivo.
		return os;
	}
}
