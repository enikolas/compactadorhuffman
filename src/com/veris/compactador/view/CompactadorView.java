package com.veris.compactador.view;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.veris.compactador.huffman.Huffman;
import com.veris.compactador.huffman.IHuffman;

@SuppressWarnings("serial")
public class CompactadorView extends JFrame {

	private JLabel lbEscolhaArquivo;
	private JFileChooser fcArquivoOrigem;
	private JTextField tfArquivoOrigem;
	private JButton btCompactar;
	private JButton btDescompactar;
	private JButton btEscolherArquivo;

	public CompactadorView() {
		super("Compactador Huffman");

		criarTela();
	}

	private void criarTela() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new FlowLayout(FlowLayout.LEFT));

		btCompactar = new JButton("Compactar");
		btCompactar.addActionListener(compactarListener);
		btCompactar.setEnabled(false);

		btDescompactar = new JButton("Descompactar");
		btDescompactar.addActionListener(descompactarListener);
		btDescompactar.setEnabled(false);

		btEscolherArquivo = new JButton("", new ImageIcon("icons/TreeOpen.gif"));
		btEscolherArquivo.addActionListener(escolherArquivoListener);

		tfArquivoOrigem = new JTextField();
		tfArquivoOrigem.setEditable(false);
		tfArquivoOrigem.setEnabled(false);

		lbEscolhaArquivo = new JLabel("Arquivo de origem:");

		fcArquivoOrigem = new JFileChooser();

		super.add(criarFormulario());

		setLocationRelativeTo(null);
		pack();
	}

	private JPanel criarFormulario() {
		FormLayout layout = new FormLayout(
				"2dlu, 100dlu, 5dlu, 85dlu,15dlu, 2dlu", // colunas
				"2dlu, pref, 2dlu, pref, 5dlu, pref, 2dlu"); // linhas

		JPanel jPanel = new JPanel(layout);
		jPanel.setBorder(BorderFactory.createTitledBorder(""));

		CellConstraints c = new CellConstraints();

		jPanel.add(lbEscolhaArquivo, c.xyw(2, 2, 3));
		jPanel.add(tfArquivoOrigem, c.xyw(2, 4, 3));
		jPanel.add(btEscolherArquivo, c.xy(5, 4));
		jPanel.add(btCompactar, c.xy(2, 6));
		jPanel.add(btDescompactar, c.xyw(4, 6, 2));

		return jPanel;
	}

	private ActionListener compactarListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			IHuffman huffman = new Huffman();
			try {
				huffman.codificar(
						new FileInputStream(fcArquivoOrigem.getSelectedFile()),
						new FileOutputStream(tfArquivoOrigem.getText()
								+ Huffman.EXTENSAO));
			} catch (FileNotFoundException e1) {
				JOptionPane.showMessageDialog(null,
						"Ocorreu um erro ao tentar ler o arquivo: "
								+ tfArquivoOrigem.getText());
				e1.printStackTrace();
			} finally {
				tfArquivoOrigem.setText("");
				btCompactar.setEnabled(false);
				btDescompactar.setEnabled(false);
			}
		}
	};

	private ActionListener descompactarListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			IHuffman huffman = new Huffman();
			try {
				huffman.decodificar(
						new FileInputStream(fcArquivoOrigem.getSelectedFile()),
						new FileOutputStream(tfArquivoOrigem.getText()
								.replaceAll(Huffman.EXTENSAO + "$", ".new")));
			} catch (FileNotFoundException e1) {
				JOptionPane.showMessageDialog(null,
						"Ocorreu um erro ao tentar ler o arquivo: "
								+ tfArquivoOrigem.getText());
				e1.printStackTrace();
			} finally {
				tfArquivoOrigem.setText("");
				btCompactar.setEnabled(false);
				btDescompactar.setEnabled(false);
			}
		}
	};

	private ActionListener escolherArquivoListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			int returnVal = fcArquivoOrigem
					.showOpenDialog(CompactadorView.this);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fcArquivoOrigem.getSelectedFile();
				tfArquivoOrigem.setText(file.getAbsolutePath());
				btCompactar.setEnabled(true);
				btDescompactar.setEnabled(true);
			}
		}
	};

}
