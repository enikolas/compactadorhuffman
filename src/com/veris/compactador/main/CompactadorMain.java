/**
 * 
 */
package com.veris.compactador.main;

import javax.swing.UIManager;

import com.veris.compactador.view.CompactadorView;


/**
 * @author enikolas
 *
 */
public class CompactadorMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e2) {}
		
		CompactadorView tela = new CompactadorView();
		tela.setVisible(true);
	}

}
